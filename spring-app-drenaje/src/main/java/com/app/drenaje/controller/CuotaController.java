package com.app.drenaje.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.app.drenaje.model.entity.Cuota;
import com.app.drenaje.model.entity.FechasPago;
import com.app.drenaje.model.entity.Socio;
import com.app.drenaje.model.entity.Usuario;
import com.app.drenaje.model.service.ICuotaService;
import com.app.drenaje.model.service.ISocioService;
import com.app.drenaje.model.service.IUsuarioService;

@RestController
@RequestMapping("/api-drenaje")
public class CuotaController {

	@Autowired
	private ICuotaService cuotaService;
	@Autowired
	private IUsuarioService usuarioService;
	@Autowired
	private ISocioService socioService;

	@PostMapping("/cuota")
	private ResponseEntity<?> store(@Valid @RequestBody Cuota cuota, BindingResult errores, OAuth2Authentication auth) {

		Map<String, Object> response = new HashMap<>();
		Cuota cuotaNew = new Cuota();
		Usuario usuario = usuarioService.findByUsername(auth.getName());

		if (errores.hasErrors()) {
			List<String> errors = errores.getFieldErrors().stream()
					.map(err -> "El campo " + err.getField() + " " + err.getDefaultMessage())
					.collect(Collectors.toList());
			response.put("errors", errors);

			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}

		Double cantidad = 0.00;
		for (FechasPago fp : cuota.getTipoCuota().getItems()) {
			cantidad += fp.getCantidad();
		}

		try {
			List<Socio> socios = socioService.findAllByStatus("A");
			List<Cuota> addCuotas = new ArrayList<Cuota>();

			for (Socio socio : socios) {

				Cuota addC = new Cuota();
				addC.setSocio(socio);
				addC.setCantidad(cantidad);
				addC.setNoPagos(cuota.getTipoCuota().getNoPagos());
				addC.setRestante(cantidad);
				addC.setTipoCuota(cuota.getTipoCuota());
				addC.setUsuario(usuario);
				addCuotas.add(addC);

			}

			cuotaService.store(addCuotas);

		} catch (DataAccessException e) {
			response.put("mensaje", "Error al registrar");
			response.put("err", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));

			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		response.put("cuota", cuotaNew);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);

	}

	@PostMapping("/cuota/cuota/individual")
	private ResponseEntity<?> storeCuotaIndidual(@Valid @RequestBody Cuota cuota, BindingResult errores,
			OAuth2Authentication auth) {

		Map<String, Object> response = new HashMap<>();
		Cuota cuotaNew = new Cuota();
		Usuario usuario = usuarioService.findByUsername(auth.getName());

		if (errores.hasErrors()) {
			List<String> errors = errores.getFieldErrors().stream()
					.map(err -> "El campo " + err.getField() + " " + err.getDefaultMessage())
					.collect(Collectors.toList());
			response.put("errors", errors);

			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}

		try {
			
			List<Cuota> addCuotas = new ArrayList<Cuota>();
			
			cuota.setNoPagos(cuota.getTipoCuota().getNoPagos());
			cuota.setRestante(cuota.getCantidad());
			cuota.setUsuario(usuario);
			addCuotas.add(cuota);

			cuotaService.store(addCuotas);

		} catch (DataAccessException e) {
			response.put("mensaje", "Error al registrar");
			response.put("err", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));

			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		response.put("cuota", cuotaNew);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}

	@GetMapping("/cuota")
	private List<Cuota> findByEstadoCuota() {
		return cuotaService.findByStatusEstadoCuota();
	}

	@GetMapping("/cuota/filtrar-cuota/{term}")
	@ResponseStatus(HttpStatus.OK)
	private List<Cuota> findByTerm(@PathVariable String term) {
		return cuotaService.findCuotaFilterBySocioNombre(term);
	}

	@GetMapping("/cuota/tipo-cuota/{tipoCuota}")
	@ResponseStatus(HttpStatus.OK)
	private List<Cuota> findByTipoCuota(@PathVariable String tipoCuota) {
		return cuotaService.findAllByTipoCuota(tipoCuota);
	}

	@GetMapping("/cuota/tipo/all/cuota/{tipoCuota}")
	@ResponseStatus(HttpStatus.OK)
	private List<Cuota> findAllCuotaByTipoCuota(@PathVariable String tipoCuota) {
		return cuotaService.findAllCuotaByTipoCuota(tipoCuota);
	}
	
	@GetMapping("/cuota/update/estado/pago")
	@ResponseStatus(HttpStatus.OK)
	private List<Cuota> updateestadoPago() {
		
		List<Cuota> getArrCuotas =cuotaService.findByStatusEstadoCuota();
		List<Cuota> arrCuotasUpdted= new ArrayList<Cuota>();
		
		for (Cuota cuota : getArrCuotas) {
			cuota.setEstadoPago("P");
			arrCuotasUpdted.add(cuota);
		}
		return cuotaService.store(arrCuotasUpdted);
	}
	
	@GetMapping("/cuota/find-estado-pago/pendiente/{tipoCuota}")
	@ResponseStatus(HttpStatus.OK)
	private List<Cuota> findByEstadoCuotaPendiente(@PathVariable String tipoCuota){
		return cuotaService.findByEstadoCuotaPendiente(tipoCuota);
	}
}
