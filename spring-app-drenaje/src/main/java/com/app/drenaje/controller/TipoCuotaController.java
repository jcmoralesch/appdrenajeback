package com.app.drenaje.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.app.drenaje.model.entity.TipoCuota;
import com.app.drenaje.model.service.ITipoCuotaService;

@RestController
@RequestMapping("/api-drenaje")
public class TipoCuotaController {

	@Autowired
	private ITipoCuotaService tipoCuotaService;
	
	
	@PostMapping("/tipo-cuota")
	private ResponseEntity<?> store(@Valid @RequestBody TipoCuota tipoCuota, BindingResult errores){
		
		Map<String, Object> response= new HashMap<>();
		TipoCuota tipoCuotaNew= new TipoCuota();
		
		if(errores.hasErrors()) {
			List<String> errors=errores.getFieldErrors().stream()
					.map(err -> "El campo " + err.getField() + " " + err.getDefaultMessage())
					.collect(Collectors.toList());;
			
			response.put("errors", errors);
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.BAD_REQUEST);
		}
		
		tipoCuota.setTipoCuota(tipoCuota.getTipoCuota().toUpperCase());
		
		try {
			
			tipoCuotaNew=tipoCuotaService.store(tipoCuota);
			
		}catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar el registro en la BBDD");
			response.put("err", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));

			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put("tipoCuota", tipoCuotaNew);
		
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.CREATED);
		
	}
	
	@GetMapping("/tipo-cuota")
	private List<TipoCuota> getAll(){
		return tipoCuotaService.getAll();
	}

}
