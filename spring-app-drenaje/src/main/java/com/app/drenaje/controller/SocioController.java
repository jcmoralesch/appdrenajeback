package com.app.drenaje.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.app.drenaje.model.entity.Socio;
import com.app.drenaje.model.service.ISocioService;


@RestController
@RequestMapping("/api-drenaje")
public class SocioController {
	
	@Autowired
	private ISocioService socioService;
	
	@PostMapping("/socio")
	private ResponseEntity<?> store(@Valid @RequestBody Socio socio, BindingResult errores) {
		
		Map<String, Object> response=new HashMap<>();
		Socio socioNew= new Socio();
		
		if(errores.hasErrors()) {
			List<String> errors=errores.getFieldErrors().stream()
					.map(err -> "El campo " + err.getField() + " " + err.getDefaultMessage())
					.collect(Collectors.toList());
			
			response.put("errors", errors);
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.BAD_REQUEST);
		}
		
		socio.setNombre(socio.getNombre().toUpperCase());
		socio.setApellido(socio.getApellido().toUpperCase());
		
		try {
			
			socioNew=socioService.store(socio);
		}
		catch (DataIntegrityViolationException ex) {
			response.put("mensaje", "Error al registrar");
			response.put("err", "El código: ".concat(socio.getCodigo()).concat(" Ya esta registrada"));

			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar el registro en la BBDD");
			response.put("err", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));

			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		response.put("socio", socioNew);		
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.CREATED);
	}
	
	@GetMapping("/socio")
	private List<Socio> getAll(){
		return socioService.findAllByStatus("A");
	}
	
	@GetMapping("/socio/sector/{sector}")
	private List<Socio> findAllBySector(@PathVariable Integer sector){
		return socioService.findAllBySector(sector);
	}
	
	@GetMapping("/socio/{genero}")
	private List<Socio> findAllByGenero(@PathVariable String genero){
		return socioService.findAllByGenero(genero);
	}
	
	@GetMapping("/socio/genero-sector/{genero}/{sector}")
	private List<Socio> findAllByGeneroAndSector(@PathVariable String genero,@PathVariable Integer sector){
		return socioService.findAllBySectorAndSexo(genero, sector);
	}
	
	@GetMapping("/socio/find-socio/{id}")
	private Socio findById(@PathVariable Long id) {
		return socioService.findById(id);
	}
	
	@PutMapping("/socio/{id}")
	private ResponseEntity<?> updateSocio(@Valid @RequestBody Socio socio,@PathVariable Long id,BindingResult errores){
		
		Socio socioUpdate=socioService.findById(id);
		Map<String, Object> response= new HashMap<>();
		
		if(errores.hasErrors()) {
			List<String> errors=errores.getFieldErrors().stream()
					.map(err -> "El campo " + err.getField() + " " + err.getDefaultMessage())
					.collect(Collectors.toList());
			
			response.put("errors", errors);
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.BAD_REQUEST);
		}
		
		socioUpdate.setNombre(socio.getNombre().toUpperCase());
		socioUpdate.setApellido(socio.getApellido().toUpperCase());
		socioUpdate.setCodigo(socio.getCodigo());
		socioUpdate.setSector(socio.getSector());
		socioUpdate.setSexo(socio.getSexo());
		socioUpdate.setTelefono(socio.getTelefono());
		
		try {		
			socioService.store(socioUpdate);		
		}catch(DataAccessException e) {
			
			response.put("mensaje", "Error al realizar el registro en la BBDD");
			response.put("err", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));

			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
			
		}
		
		response.put("socio", socioUpdate);	
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.CREATED);
	}
}
