package com.app.drenaje.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.app.drenaje.model.entity.Sector;
import com.app.drenaje.model.service.ISectorService;

@RestController
@RequestMapping("/api-drenaje")
public class SectorController {
	
	@Autowired
	private ISectorService sectorService;
	
	@GetMapping("/sector")
	private List<Sector> getAll(){
		
		return sectorService.getAll();
	}

}
