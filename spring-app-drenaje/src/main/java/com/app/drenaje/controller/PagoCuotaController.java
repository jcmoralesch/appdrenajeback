package com.app.drenaje.controller;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.app.drenaje.model.entity.Cuota;
import com.app.drenaje.model.entity.PagoCuota;
import com.app.drenaje.model.entity.Socio;
import com.app.drenaje.model.entity.Usuario;
import com.app.drenaje.model.service.ICuotaService;
import com.app.drenaje.model.service.IPagoCuotaService;
import com.app.drenaje.model.service.IUsuarioService;


@RestController
@RequestMapping("/api-drenaje")
public class PagoCuotaController {
	
	@Autowired
	private IPagoCuotaService pagoCuotaService;
	@Autowired
	private IUsuarioService usuarioService;
	@Autowired
	private ICuotaService cuotaService;
	
	@PostMapping("/pago-cuota")
	private ResponseEntity<?> store(@Valid @RequestBody List<PagoCuota> pagoCuota, 
			                        BindingResult errores,OAuth2Authentication auth){
		
		Map<String, Object> response= new HashMap<>();
		Usuario usuario = usuarioService.findByUsername(auth.getName());
		List<PagoCuota> pagosInsertados= new ArrayList<PagoCuota>();
		
		
		if(errores.hasErrors()) {
			List<String> errors=errores.getFieldErrors().stream()
					.map(err -> "El campo " + err.getField() + " " + err.getDefaultMessage())
					.collect(Collectors.toList());
			
			response.put("errors", errors);
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.BAD_REQUEST);
		}
		
		try {
			
			List<Cuota> actualizarCuota=new ArrayList<Cuota>();
			List<PagoCuota> addUsuario= new ArrayList<PagoCuota>();
			
			for (PagoCuota pG : pagoCuota) {
				
				Cuota cuota=cuotaService.fidById(pG.getCuota().getId());
				Double calcularRestante=cuota.getRestante()-pG.getCantidad();
				Double calcularPagado=pG.getCantidad()+cuota.getTotalPagado();
				
				cuota.setEstadoPago("C");
				cuota.setRestante(calcularRestante);
				cuota.setTotalPagado(calcularPagado);
				pG.setUsuario(usuario);
				
				if(calcularRestante==0.00) {
					cuota.setEstadoCuota("Cancelado");
				}
				
				addUsuario.add(pG);
				actualizarCuota.add(cuota);		
			}
			pagosInsertados = pagoCuotaService.storePagoUpdateCuota(addUsuario, actualizarCuota);
		}
		catch(DataAccessException e) {
			
			response.put("mensaje", "Error en el servidor al realizar el pago");
			response.put("err", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			
			return  new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
			
		}
		
		response.put("pagoCuota",pagosInsertados);
		
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.CREATED);
	}
	
	@GetMapping("/pago-cuota")
	private List<PagoCuota> findByFechaPago(){
		LocalDateTime fh = LocalDateTime.now().minusHours(6);	
		LocalDate fechaActual =  LocalDate.of(fh.getYear(), fh.getMonthValue(), fh.getDayOfMonth()); //Se calcula fecha de la siguiente manera por diferencia de zona horaria en docker
		return pagoCuotaService.findByFechaPago(fechaActual);
	}
	
	@GetMapping("/pago-cuota/find/{fecha1}/{fecha2}/{tipoCuota}")
	public List<PagoCuota> findByDates(@PathVariable("fecha1") String fecha1, @PathVariable("fecha2")String fecha2, @PathVariable("tipoCuota")String tipoCuota){
		
		LocalDate f1=LocalDate.parse(fecha1);
		LocalDate f2= LocalDate.parse(fecha2);
		
		if(tipoCuota.equals("undefined")) {
			tipoCuota="";
		}
		
		if(tipoCuota.equals("")) {

			return pagoCuotaService.finByFechaBetween(f1, f2); 
		}
		
		if(tipoCuota!="") {
			
			return pagoCuotaService.findPagoCuotaByDateAndTipoCuota(f1, f2, tipoCuota);
		}
		return null;
	}
	
	@PostMapping("/pago-cuota/find/by/socio")
	public List<PagoCuota> findAllBySocio(@RequestBody Socio socio){
		
		Long id = socio.getId();
		
		return pagoCuotaService.findAllByCuotaSocioId(id);
	}

}
