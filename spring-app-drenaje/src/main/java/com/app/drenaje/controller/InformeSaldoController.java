package com.app.drenaje.controller;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.app.drenaje.model.entity.InformeSaldo;
import com.app.drenaje.model.entity.Usuario;
import com.app.drenaje.model.service.IInformeSaldoService;
import com.app.drenaje.model.service.IItemGastoService;
import com.app.drenaje.model.service.IPagoCuotaService;
import com.app.drenaje.model.service.IUsuarioService;

@RestController
@RequestMapping("/api-drenaje")
public class InformeSaldoController {
	
	@Autowired
	private IInformeSaldoService informeSaldoService;
	@Autowired
	private IPagoCuotaService pagoCuotaService;
	@Autowired
	private IItemGastoService itemGastoService;
	@Autowired
	private IUsuarioService usuarioService;
	
	@GetMapping("/informe-saldo")
	@ResponseStatus(HttpStatus.OK)
	private InformeSaldo registraInforme( OAuth2Authentication auth) {
		
		Integer verificar=informeSaldoService.countInformeSaldo();
		InformeSaldo informeSaldo = new InformeSaldo();
		Usuario usuario= usuarioService.findByUsername(auth.getName());
		
		if(verificar==0) {
			LocalDateTime fh = LocalDateTime.now().minusHours(6);//Se calcula fecha de la siguiente manera por diferencia de zona horaria en docker	
			informeSaldo.setFechaLectura(LocalDate.of(fh.getYear(), fh.getMonthValue(), fh.getDayOfMonth()));
			informeSaldo.setSaldoAnterior(0.00);
			informeSaldo.setSaldoEnCaja(pagoCuotaService.sumCantidad()-itemGastoService.sumTotal());
			informeSaldo.setTotalGasto(itemGastoService.sumTotal());
			informeSaldo.setTotalRecaudo(pagoCuotaService.sumCantidad());
			informeSaldo.setUsuario(usuario);
			
		}
		else {
			Long id=informeSaldoService.getByMaxId();
			InformeSaldo infSaldo=informeSaldoService.findById(id);
			Double recaudo=pagoCuotaService.sumCantidad()-informeSaldoService.sumTotalRecaudo();
			System.out.println(recaudo);
			System.out.println(pagoCuotaService.sumCantidad());
			System.out.println(informeSaldoService.sumTotalRecaudo());
			
			Double gasto=itemGastoService.sumTotal()-informeSaldoService.sumTotalGasto();
	
			informeSaldo.setFechaLectura(infSaldo.getFecha());
			informeSaldo.setSaldoAnterior(infSaldo.getSaldoEnCaja());
			informeSaldo.setTotalRecaudo(recaudo);
			informeSaldo.setTotalGasto(gasto);
			informeSaldo.setSaldoEnCaja(infSaldo.getSaldoEnCaja()+recaudo-gasto);
			informeSaldo.setUsuario(usuario);
			
		}
		
		informeSaldoService.store(informeSaldo);
		
		return new InformeSaldo();
	}
	
	@GetMapping("/informe-saldo/reporte")
	@ResponseStatus(HttpStatus.OK)
	private InformeSaldo getByMax() {
		Long id=informeSaldoService.getByMaxId();
		if(id!=null) {
			InformeSaldo infSaldo=informeSaldoService.findById(id);
			return infSaldo;
		}
		
		return null;	
	}
	
	@GetMapping("/informe-saldo/find/all")
	@ResponseStatus(HttpStatus.OK)
	private List<InformeSaldo> getAllOrderByDesc(){
		return informeSaldoService.getAllOrderByDesc();
	}
	

}
