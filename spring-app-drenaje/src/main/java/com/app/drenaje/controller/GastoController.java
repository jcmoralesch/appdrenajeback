package com.app.drenaje.controller;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.app.drenaje.model.entity.Gasto;
import com.app.drenaje.model.entity.Usuario;
import com.app.drenaje.model.service.IGastoService;
import com.app.drenaje.model.service.IItemGastoService;
import com.app.drenaje.model.service.IPagoCuotaService;
import com.app.drenaje.model.service.IUsuarioService;

@RestController
@RequestMapping("/api-drenaje")
public class GastoController {
	
	@Autowired
	private IGastoService gastoService;
	@Autowired
	private IUsuarioService usuarioService;
	@Autowired
	private IPagoCuotaService pagoCuotaService;
	@Autowired
	private IItemGastoService itemGastoService;
	
	
	@PostMapping("/gasto")
	private ResponseEntity<?> store(@Valid @RequestBody Gasto gasto, BindingResult errores, OAuth2Authentication auth){
	    Map<String, Object> response= new HashMap<>();
	    Usuario usuario= usuarioService.findByUsername(auth.getName());
	    Gasto gastoNew= new Gasto();
	    
	    if(errores.hasErrors()) {
	    	List<String> errors= errores.getFieldErrors().stream()
					.map(err -> "El campo " + err.getField() + " " + err.getDefaultMessage())
					.collect(Collectors.toList());
	    	
	    	response.put("errors", errors);
	    	
	    	return new ResponseEntity<Map<String,Object>>(response,HttpStatus.BAD_REQUEST); 	
	    }
	    
	    try {
	    	
	    	gasto.setUsuario(usuario);
	    	gastoNew=gastoService.store(gasto);
	    	
	    }
	    catch(DataAccessException e) {
	    	
	    	response.put("mensaje","Error al realizar la insercion en la Base de datos");
	    	response.put("err", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
	    	
	    	return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);	
	    } 
	    response.put("gasto", gastoNew);
	    return new ResponseEntity<Map<String,Object>>(response,HttpStatus.CREATED);
	}
	
	@GetMapping("/gasto/curren-date")
	@ResponseStatus(HttpStatus.OK)
	private List<Gasto> getByCurrendDate(){
		LocalDateTime fh = LocalDateTime.now().minusHours(6);	
		LocalDate fechaActual =  LocalDate.of(fh.getYear(), fh.getMonthValue(), fh.getDayOfMonth()); //Se calcula fecha de la siguiente manera por diferencia de zona horaria en docker
		return gastoService.findByFechaCurrentDate(fechaActual);
	}
	
	@GetMapping("/gasto/find/{fecha1}/{fecha2}")
	public List<Gasto> findByDates(@PathVariable("fecha1") String fecha1, @PathVariable("fecha2")String fecha2){
		
		LocalDate f1=LocalDate.parse(fecha1);
		LocalDate f2= LocalDate.parse(fecha2);
		
	    return gastoService.findAllByFechaBetween(f1, f2); 

	}
	
	@GetMapping("/gasto/consultar/disponible")
	private ResponseEntity<?> consultarDisponible(){
		Map<String, Object> response=new HashMap<>();
		Double totalDisponible=pagoCuotaService.sumCantidad()-itemGastoService.sumTotal();
		
		response.put("totalRecaudo", pagoCuotaService.sumCantidad());
		response.put("totalGasto", itemGastoService.sumTotal());
		response.put("totalDisponible",totalDisponible);		
		
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.OK);
	}

}
