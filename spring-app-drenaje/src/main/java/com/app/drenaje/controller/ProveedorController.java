package com.app.drenaje.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.app.drenaje.model.entity.Proveedor;
import com.app.drenaje.model.service.IProveedorService;

@RestController
@RequestMapping("/api-drenaje")
public class ProveedorController {
	
	@Autowired
	private IProveedorService proveedorService;
	
	@PostMapping("/proveedor")
	private ResponseEntity<?> store(@Valid @RequestBody Proveedor proveedor,BindingResult errores) {
		
		Map<String, Object> response=new HashMap<>();
		Proveedor proveedorNew= new Proveedor();
		
		if(errores.hasErrors()) {
			List<String> errors= errores.getFieldErrors().stream()
					.map(err -> "El campo " + err.getField() + " " + err.getDefaultMessage())
					.collect(Collectors.toList());
	    	
	    	response.put("errors", errors);
	    	
	    	return new ResponseEntity<Map<String,Object>>(response,HttpStatus.BAD_REQUEST);
		}
		
		try {
			
			proveedor.setNombreCompleto(proveedor.getNombreCompleto().toUpperCase());
			proveedor.setDescripcion(proveedor.getDescripcion().toUpperCase());
			proveedor.setDireccion(proveedor.getDireccion().toUpperCase());
			
			proveedorNew=proveedorService.store(proveedor);	
		}	
		catch(DataAccessException e) {
			response.put("mensaje","Error al realizar la insercion en la Base de datos");
	    	response.put("err", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
	    	
	    	return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put("proveedor", proveedorNew);
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.CREATED);
	}
	
	
	@GetMapping("/proveedor/filtrar-proveedor/{term}")
	@ResponseStatus(HttpStatus.OK)
	private List<Proveedor> filterByNobreCompleto(@PathVariable String term){
		return proveedorService.findByNombreCompleto(term);
	}
}
