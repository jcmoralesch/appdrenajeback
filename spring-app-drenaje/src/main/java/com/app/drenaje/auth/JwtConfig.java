package com.app.drenaje.auth;

public class JwtConfig {
	
	public static final String RSA_PRIVADA="-----BEGIN RSA PRIVATE KEY-----\n" + 
			"MIIEpAIBAAKCAQEAzi80cgpeCO7mOdLoymhdsYSmsYg9bExw0CzRdnbaZqaBkBOh\n" + 
			"i5zcsrh2zqeFTIq6dvjng89zkULNwhXrUsuOKi0LnTTLz39U8lbistDmn4Cd3TeP\n" + 
			"ZpKpaJmembjqNLhhNUx26EbQxyDZLUTa7ym/CZJDUiHQKui3PZq8fChiL6RJAQ1B\n" + 
			"6+HtnQWKtpnHPf1qz9ng/Mm7+uVV1+ztnqYL80RTSCWNzEey+ipJFTF8ad+WEWfj\n" + 
			"vaRbleMLh5/I1ClzGoELO6ojLrMOuBTkuj4BnZreqVuBdSWdXHYgs/DEJzWtRqTC\n" + 
			"kETRwGECVZjLPcYdEnJBigk7di/tyle8XmnU2wIDAQABAoIBAQCD6J9c9lUC0hRo\n" + 
			"mg5ksDCBCaS7n7XsO1tRU9sONyuy0IPDcUvAzjJEBM+9ll8pgPeaddNn+KivHlYU\n" + 
			"/YD4D1J8mST9BnH95jCqefu++vcQxHA6vvj3Xo0TZnSZVDxCTlOijbDlGakoPFO1\n" + 
			"KoAG0mZt8S0wtoE/cnMI7p33sL5gS0yK1Vqi3e1gGwGUTGyEQlyhrkU+zYt/gM+O\n" + 
			"KAm1V/dGfsliWM3fHTO7w/JuxCaqmQuxg63y1LLC8gmvrKCC73FFrTHmIswtWtMU\n" + 
			"BTNi9ACF30sunyZPH9BbLIbf3Fy2c83OjRTY+zmT84SvyO0H9V1Buj2f8hxWJqCd\n" + 
			"Cx+ICssBAoGBAOjDvEmvKoyzpfNmdBUNX16kcpznBIpLWJ5DuVcN8V99G9Q8YpSV\n" + 
			"pFQvBSZWF2x7Nr4ifOxMyzK39HIJQUqV2f9xeDh+ueASac4G/dzcasxm+8udXKok\n" + 
			"rRB9ofCZtph4rUIbCCCKDAz+RieSvKalk29hDZ2bZ9UHYH4SRc1tfSRxAoGBAOLE\n" + 
			"N2PXv7rMstzTHVamJZa4Tip1fZ8FMtjM3Wzd+CZwQnuerQpb1vuVBqrD2TX9m6pk\n" + 
			"vMMJ3ZIIpIV02sFcJ8NxXBuL6uyBmRzYmrjh1Yf+yrxoTKvBK1QX8s02SzCZFiSG\n" + 
			"cnVXpaFAogGrxUD367kH2lzALgynKPpej+Dar4QLAoGAUyeiO5y1kcCYV8VRUTgO\n" + 
			"yyvquj0Z9SIHzbqaGwyjH579cywDd9uh8mU97L0y7xFxtCl0AaNJsJvhW2ANEE1K\n" + 
			"rSRnc59qXi+V1wHObbnc+ZsM92gHc81Yepy3XtI7Vms+UnB/zcgjl/y7KWYVuV9L\n" + 
			"ta2xhI3BYWyaP0/d2g70dzECgYEAiggN/0RQfJAlP61x6lBwCPJfAhFKSgWMEdOB\n" + 
			"5MaoSh2DGmRXyDpm/fSUflWAXhDlNC7lh4L1z/Ja0rFyhshlm93gxxPAB/5LCPgA\n" + 
			"FhHBPGqnVU/Xe6S1jxaUwL0Pv6EvjUQZX7veIe2Jr6KzCvzeC17wWAzS+vi1RNFS\n" + 
			"mvgCZkkCgYBabUlSENhYk7G+H87orrF7BkhIR0h1kNbbMHRuyCCqfOJiSL1PzQT4\n" + 
			"ZM3tNdnWtIq7DjsQqQoAsW6Rb8y2w39rvPg2SL1eOKpy7O8/jyXmr7HHZghicDYK\n" + 
			"ClQ3IYr8RrJ7xN2Ar6DiKqqUSGSAnFHU/HtceDteUWRz7Rc8uj+TmQ==\n" + 
			"-----END RSA PRIVATE KEY-----";
	
	public static final String RSA_PUBLICA="-----BEGIN PUBLIC KEY-----\n" + 
			"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAzi80cgpeCO7mOdLoymhd\n" + 
			"sYSmsYg9bExw0CzRdnbaZqaBkBOhi5zcsrh2zqeFTIq6dvjng89zkULNwhXrUsuO\n" + 
			"Ki0LnTTLz39U8lbistDmn4Cd3TePZpKpaJmembjqNLhhNUx26EbQxyDZLUTa7ym/\n" + 
			"CZJDUiHQKui3PZq8fChiL6RJAQ1B6+HtnQWKtpnHPf1qz9ng/Mm7+uVV1+ztnqYL\n" + 
			"80RTSCWNzEey+ipJFTF8ad+WEWfjvaRbleMLh5/I1ClzGoELO6ojLrMOuBTkuj4B\n" + 
			"nZreqVuBdSWdXHYgs/DEJzWtRqTCkETRwGECVZjLPcYdEnJBigk7di/tyle8XmnU\n" + 
			"2wIDAQAB\n" + 
			"-----END PUBLIC KEY-----";

}
