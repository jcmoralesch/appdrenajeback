package com.app.drenaje.auth;

import java.util.Arrays;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter{

	@Override
	public void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
		.antMatchers(HttpMethod.GET,"/api-drenaje/usuario").hasRole("ADMIN")
		.antMatchers(HttpMethod.POST,"/api-drenaje/cuota").hasRole("ADMIN")
		.antMatchers(HttpMethod.POST,"/api-drenaje/cuota/cuota/individual").hasRole("ADMIN")
		.antMatchers(HttpMethod.GET,"/api-drenaje/cuota").hasAnyRole("ADMIN","USER")
		.antMatchers(HttpMethod.GET,"/api-drenaje/cuota/filtrar-cuota/{term}").hasRole("ADMIN")
		.antMatchers(HttpMethod.GET,"/api-drenaje/cuota/tipo-cuota/{tipoCuota}").hasAnyRole("ADMIN","USER")
		.antMatchers(HttpMethod.GET,"/api-drenaje/cuota/tipo/all/cuota/{tipoCuota}").hasRole("ADMIN")
		.antMatchers(HttpMethod.GET,"/api-drenaje/update/estado/pago").hasRole("ADMIN")
		.antMatchers(HttpMethod.GET,"/api-drenaje/cuota/find-estado-pago/pendiente/{tipoCuota}").hasAnyRole("ADMIN","USER")
		.antMatchers(HttpMethod.POST,"/api-drenaje/gasto").hasRole("ADMIN")
		.antMatchers(HttpMethod.GET,"/api-drenaje/gasto/current-date").hasRole("ADMIN")
		.antMatchers(HttpMethod.GET,"/api-drenaje/gasto/find/{fecha1}/{fecha2}").hasAnyRole("ADMIN","USER")
		.antMatchers(HttpMethod.GET,"/api-drenaje/gasto/consultar/disponible").hasAnyRole("ADMIN","USER")
		.antMatchers(HttpMethod.GET,"/api-drenaje/informe-saldo").hasRole("ADMIN")
		.antMatchers(HttpMethod.GET,"/api-drenaje/informe-saldo/reporte").hasAnyRole("ADMIN","USER")
		.antMatchers(HttpMethod.GET,"/api-drenaje/informe-saldo/find/all").hasAnyRole("ADMIN","USER")
		.antMatchers(HttpMethod.POST,"/api-drenaje/pago-cuota").hasRole("ADMIN")
		.antMatchers(HttpMethod.GET,"/api-drenaje/pago-cuota").hasAnyRole("ADMIN","USER")
		.antMatchers(HttpMethod.GET,"/api-drenaje/pago-cuota/find/{fecha1}/{fecha2}/{tipoCuota}").hasAnyRole("ADMIN","USER")
		.antMatchers(HttpMethod.POST,"/api-drenaje/proveedor").hasRole("ADMIN")
		.antMatchers(HttpMethod.GET,"/api-drenaje/proveedor/filtrar-proveedor/{term}").hasRole("ADMIN")
		.antMatchers(HttpMethod.GET,"/api-drenaje/sector").hasAnyRole("ADMIN","USER")
		.antMatchers(HttpMethod.POST,"/api-drenaje/socio").hasRole("ADMIN")
		.antMatchers(HttpMethod.GET,"/api-drenaje/socio").hasAnyRole("ADMIN","USER")
		.antMatchers(HttpMethod.GET,"/api-drenaje/socio/sector/{sector}").hasRole("ADMIN")
		.antMatchers(HttpMethod.GET,"/api-drenaje/socio/{genero}").hasRole("ADMIN")
		//.antMatchers(HttpMethod.GET,"/api-drenaje/socio").hasRole("ADMIN")
		.antMatchers(HttpMethod.GET,"/api-drenaje/socio/genero-sector/{genero}/{sector}").hasAnyRole("ADMIN","USER")
		.antMatchers(HttpMethod.GET,"/api-drenaje/socio/find-socio/{id}").hasRole("ADMIN")
		.antMatchers(HttpMethod.PUT,"/api-drenaje/socio/{id}").hasRole("ADMIN")
		.antMatchers(HttpMethod.POST,"/api-drenaje/tipo-cuota").hasRole("ADMIN")
		.antMatchers(HttpMethod.GET,"/api-drenaje/tipo-cuota").hasAnyRole("ADMIN","USER")
		//.antMatchers(HttpMethod.GET,"/api-drenaje/socio").hasRole("ADMIN")
		.antMatchers(HttpMethod.GET,"/api-drenaje/usuario/{term}").hasRole("ADMIN")
		.antMatchers(HttpMethod.POST,"/api-drenaje/usuario").hasRole("ADMIN")
		.anyRequest().authenticated()
		.and().cors().configurationSource(corsConfigurationSource());
	}
	
	@Bean
    public CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowedOrigins(Arrays.asList("http://localhost:4200","*"));
        configuration.setAllowedMethods(Arrays.asList("GET","POST","DELETE","PUT","OPTIONS"));
        configuration.setAllowCredentials(true);
        configuration.setAllowedHeaders(Arrays.asList("Content-Type","Authorization"));
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }
	
	 @Bean
	 public FilterRegistrationBean<CorsFilter>corsFilter(){
		 FilterRegistrationBean<CorsFilter> bean = new FilterRegistrationBean<CorsFilter>(new CorsFilter(corsConfigurationSource()));
		 bean.setOrder(Ordered.HIGHEST_PRECEDENCE);
		 return bean;
		 
	 }
}
