package com.app.drenaje;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
public class SpringAppDrenajeApplication implements CommandLineRunner{
	
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	

	public static void main(String[] args) {
		SpringApplication.run(SpringAppDrenajeApplication.class, args);
	}


	@Override
	public void run(String... args) throws Exception {
		
		
		  String pass="prueba2023*"; for(int i=0; i<2; i++) { String
		  p=passwordEncoder.encode(pass);
		  
		  System.out.println(p+'*');
		  }
		 
		
	}
	
	

}
