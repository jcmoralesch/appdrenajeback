package com.app.drenaje.model.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.app.drenaje.model.dao.IProveedorDao;
import com.app.drenaje.model.entity.Proveedor;

@Service
public class ProveedorServiceImpl implements IProveedorService {
	
	@Autowired
	private IProveedorDao proveedorDao;

	@Override
	@Transactional
	public Proveedor store(Proveedor proveedor) {
		
		return proveedorDao.save(proveedor);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Proveedor> findByNombreCompleto(String term) {
		
		return proveedorDao.findByNombreCompleto(term);
	}

}
