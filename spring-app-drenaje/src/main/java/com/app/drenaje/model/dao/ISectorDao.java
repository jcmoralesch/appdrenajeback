package com.app.drenaje.model.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import com.app.drenaje.model.entity.Sector;


public interface ISectorDao extends JpaRepository<Sector, Long>{

}
