package com.app.drenaje.model.service;

import java.util.List;
import com.app.drenaje.model.entity.Sector;

public interface ISectorService {
	
	public Sector store(Sector sector);
	public List<Sector> getAll();

}
