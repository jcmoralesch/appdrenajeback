package com.app.drenaje.model.dao;

import java.time.LocalDate;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import com.app.drenaje.model.entity.PagoCuota;

public interface IPagoCuotaDao extends JpaRepository<PagoCuota, Long> {
	
	@Query("select p from PagoCuota p where p.fecha=?1 order by p.id desc")
	public List<PagoCuota> findByFechaPago(LocalDate fecha1);
	public List<PagoCuota> findAllByFechaBetween(LocalDate fecha1,LocalDate fecha2);
	@Query("select p from PagoCuota p where p.fecha between ?1 and ?2 and p.cuota.tipoCuota.tipoCuota=?3")
	public List<PagoCuota> findPagoCuotaByDateAndTipoCuota(LocalDate fecha1,LocalDate fecha2,String tipoCuota);
	
	@Query("select sum(p.cantidad) from PagoCuota p")
	public Double sumCantidad();
	
	public List<PagoCuota> findAllByCuotaSocioId(Long id);

}
