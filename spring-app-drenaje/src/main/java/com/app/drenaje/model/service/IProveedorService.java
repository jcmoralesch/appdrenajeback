package com.app.drenaje.model.service;

import java.util.List;
import com.app.drenaje.model.entity.Proveedor;

public interface IProveedorService {
	
	public Proveedor store(Proveedor proveedor);
	public List<Proveedor> findByNombreCompleto(String term);

}
