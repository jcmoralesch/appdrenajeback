package com.app.drenaje.model.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.app.drenaje.model.dao.ISocioDao;
import com.app.drenaje.model.entity.Socio;

@Service
public class SocioServiceImpl implements ISocioService{
	
	@Autowired
	private ISocioDao socioDao;

	@Override
	@Transactional
	public Socio store(Socio socio) {
		
		return socioDao.save(socio);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Socio> findAllByStatus(String status) {
		
		return socioDao.findAllByStatus(status);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Socio> findAllBySector(Integer sector) {
		
		return socioDao.findAllBySector(sector);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Socio> findAllByGenero(String genero) {
		
		return socioDao.findAllByGenero(genero);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Socio> findAllBySectorAndSexo(String genero, Integer sector) {
		
		return socioDao.findAllBySectorAndSexo(genero, sector);
	}

	@Override
	@Transactional(readOnly = true)
	public Socio findById(Long id) {
		
		return socioDao.findById(id).orElse(null);
	}

}
