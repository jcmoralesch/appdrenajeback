package com.app.drenaje.model.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import com.app.drenaje.model.entity.InformeSaldo;

public interface IInformeSaldoDao extends JpaRepository<InformeSaldo, Long> {
	
	@Query("select count(*) from InformeSaldo")
	public Integer countInformeSaldo();
	@Query("select max(i.id) from InformeSaldo i")
	public Long getByMaxId();
	@Query("select sum(i.totalGasto) from InformeSaldo i")
	public Double sumTotalGasto();
	@Query("select sum(i.totalRecaudo) from InformeSaldo i")
	public Double sumTotalRecaudo();
	@Query("select i from InformeSaldo i order by i.id desc")
	public List<InformeSaldo> getAllOrderByDesc();

}
