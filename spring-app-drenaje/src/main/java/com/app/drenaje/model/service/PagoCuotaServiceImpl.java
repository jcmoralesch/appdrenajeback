package com.app.drenaje.model.service;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.drenaje.model.dao.ICuotaDao;
import com.app.drenaje.model.dao.IPagoCuotaDao;
import com.app.drenaje.model.entity.Cuota;
import com.app.drenaje.model.entity.PagoCuota;

@Service
public class PagoCuotaServiceImpl implements IPagoCuotaService{
	
	@Autowired
	private IPagoCuotaDao pagoCuotaDao;
	@Autowired
	private ICuotaDao cuotaDao;

	@Override
	@Transactional(readOnly = true)
	public List<PagoCuota> findByFechaPago(LocalDate fecha1) {
		
		return pagoCuotaDao.findByFechaPago(fecha1);
	}

	@Override
	@Transactional
	public List<PagoCuota> storePagoUpdateCuota(List<PagoCuota> pagoCuota, List<Cuota> cuota) {
		cuotaDao.saveAll(cuota);
		return pagoCuotaDao.saveAll(pagoCuota);
	}

	@Override
	@Transactional(readOnly = true)
	public List<PagoCuota> finByFechaBetween(LocalDate fecha1, LocalDate fecha2) {
		
		return pagoCuotaDao.findAllByFechaBetween(fecha1, fecha2);
	}

	@Override
	@Transactional(readOnly = true)
	public List<PagoCuota> findPagoCuotaByDateAndTipoCuota(LocalDate fecha1, LocalDate fecha2, String tipoCuota) {
		
		return pagoCuotaDao.findPagoCuotaByDateAndTipoCuota(fecha1, fecha2, tipoCuota);
	}

	@Override
	public Double sumCantidad() {
		
		return pagoCuotaDao.sumCantidad();
	}

	@Override
	public List<PagoCuota> findAll() {
		
		return pagoCuotaDao.findAll();
	}

	@Override
	public List<PagoCuota> findAllByCuotaSocioId(Long id) {
		
		return pagoCuotaDao.findAllByCuotaSocioId(id);
	}

}
