package com.app.drenaje.model.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.DecimalMin;

@Entity
@Table(name = "pago_cuotas")
public class PagoCuota implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	//@NotEmpty
	@DecimalMin(value = "0.00")
	@Column(nullable = false)
	private Double cantidad;
	private LocalDate fecha;
	private LocalTime hora;
	//@NotEmpty
	@ManyToOne(fetch = FetchType.LAZY)
	private Cuota cuota;
	@ManyToOne(fetch = FetchType.LAZY)
	private Usuario usuario;

	@PrePersist
	private void prePersist() {
		//fecha = LocalDate.now(ZoneId.of("America/Guatemala"));
		//hora = LocalTime.now(ZoneId.of("America/Guatemala"));
		
		//*************Se calcula fecha y hora de las siguiente manera por diferencia de zona horaria en docker*********************************
		LocalDateTime fh = LocalDateTime.now().minusHours(6);	
		fecha =  LocalDate.of(fh.getYear(), fh.getMonthValue(), fh.getDayOfMonth());
		hora = LocalTime.of(fh.getHour(), fh.getMinute(), fh.getSecond(), fh.getNano());
		//**************************************************************************************************************************************
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getCantidad() {
		return cantidad;
	}

	public void setCantidad(Double cantidad) {
		this.cantidad = cantidad;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public LocalTime getHora() {
		
		return hora;
	}

	public void setHora(LocalTime hora) {
		this.hora = hora;
	}

	public Cuota getCuota() {
		return cuota;
	}

	public void setCuota(Cuota cuota) {
		this.cuota = cuota;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
}
