package com.app.drenaje.model.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.app.drenaje.model.entity.Socio;

public interface ISocioDao extends JpaRepository<Socio, Long> {
	
	@Query("select s from Socio s where s.sector.sector=?1 and s.status='A'")
	public List<Socio> findAllBySector(Integer sector);
	public List<Socio> findAllByStatus(String status);
	@Query("select s from Socio s where s.sexo=?1 and s.status='A'")
	public List<Socio> findAllByGenero(String genero);
	@Query("select s from Socio s where s.sexo=?1 and s.sector.sector=?2 and s.status='A'")
	public List<Socio> findAllBySectorAndSexo(String genero,Integer sector);
	
}
