package com.app.drenaje.model.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

@Entity
@Table(name = "gasto_items")
public class ItemGasto implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Min(value = 0)
	private Integer cantidad;
	@NotEmpty
	@Pattern(regexp = "[ A-Za-z0-9,äÄëËïÏöÖüÜáéíóúáéíóúÁÉÍÓÚÂÊÎÔÛâêîôûàèìòùÀÈÌÒÙ-]+")
	@Column(length = 60, nullable = false)
	private String descripcion;
	@DecimalMin(value = "0.00")
	@Column(name = "precio_unitario", nullable = false)
	private Double precioUnitario;
	@DecimalMin(value = "0.00")
	@Column(name = "sub_total_compra", nullable = false)
	private Double subTotalCompra;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Double getPrecioUnitario() {
		return precioUnitario;
	}

	public void setPrecioUnitario(Double precioUnitario) {
		this.precioUnitario = precioUnitario;
	}

	public Double getSubTotalCompra() {
		return subTotalCompra;
	}

	public void setSubTotalCompra(Double subTotalCompra) {
		this.subTotalCompra = subTotalCompra;
	}

	/*
	public Double getImporte() {
		return cantidad.doubleValue() * subTotalCompra;
	}*/

}
