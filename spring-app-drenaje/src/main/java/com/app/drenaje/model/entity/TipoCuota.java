package com.app.drenaje.model.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "tipo_cuotas")
public class TipoCuota implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@NotEmpty
	@Column(name = "tipo_cuota", unique = true, length = 30,nullable = false)
	private String tipoCuota;
	@Column( length = 30,nullable = false)
	private Integer noPagos;
	@NotEmpty
	@Column(length = 60,nullable = false)
	private String descripcion;
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler","items" })
	@JoinColumn(name = "tipo_cuota_id")
	private List<FechasPago> items;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTipoCuota() {
		return tipoCuota;
	}

	public void setTipoCuota(String tipoCuota) {
		this.tipoCuota = tipoCuota;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public List<FechasPago> getItems() {
		return items;
	}

	public void setItems(List<FechasPago> items) {
		this.items = items;
	}

	public Integer getNoPagos() {
		return noPagos;
	}

	public void setNoPagos(Integer noPagos) {
		this.noPagos = noPagos;
	}
	
	
}
