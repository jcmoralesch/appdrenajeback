package com.app.drenaje.model.service;

import java.time.LocalDate;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.drenaje.model.dao.IGastoDao;
import com.app.drenaje.model.entity.Gasto;

@Service
public class GastoServiceImpl implements IGastoService{
	
	@Autowired
	private IGastoDao gastoDao;

	@Override
	@Transactional
	public Gasto store(Gasto gasto) {
		
		return gastoDao.save(gasto);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Gasto> findByFechaCurrentDate(LocalDate fecha1) {
	
		return gastoDao.findByFechaCurrentDate(fecha1);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Gasto> findAllByFechaBetween(LocalDate fecha1, LocalDate fecha2) {
		
		return gastoDao.findAllByFechaBetween(fecha1, fecha2);
	}
}
