package com.app.drenaje.model.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;

@Entity
@Table(name = "informe_saldos")
public class InformeSaldo implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name = "saldo_anterior", nullable = false)
	private Double saldoAnterior;
	@Column(name = "total_recaudo", nullable = false)
	private Double totalRecaudo;
	@Column(name = "total_gasto", nullable = false)
	private Double totalGasto;
	@Column(name = "saldo_en_caja", nullable = false)
	private Double saldoEnCaja;
	private LocalDate fecha;
	private LocalDate fechaLectura;
	@ManyToOne(fetch = FetchType.LAZY)
	private Usuario usuario;

	@PrePersist()
	private void prePersist() {
		// this.fecha=LocalDate.now(ZoneId.of("America/Guatemala"));

		// *************Se calcula fecha y hora de las siguiente manera por diferencia de zona horaria en docker*********************************
		LocalDateTime fh = LocalDateTime.now().minusHours(6);
		fecha = LocalDate.of(fh.getYear(), fh.getMonthValue(), fh.getDayOfMonth());
		// **************************************************************************************************************************************
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getSaldoAnterior() {
		return saldoAnterior;
	}

	public void setSaldoAnterior(Double saldoAnterior) {
		this.saldoAnterior = saldoAnterior;
	}

	public Double getTotalRecaudo() {
		return totalRecaudo;
	}

	public void setTotalRecaudo(Double totalRecaudo) {
		this.totalRecaudo = totalRecaudo;
	}

	public Double getTotalGasto() {
		return totalGasto;
	}

	public void setTotalGasto(Double totalGasto) {
		this.totalGasto = totalGasto;
	}

	public Double getSaldoEnCaja() {
		return saldoEnCaja;
	}

	public void setSaldoEnCaja(Double saldoEnCaja) {
		this.saldoEnCaja = saldoEnCaja;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public LocalDate getFechaLectura() {
		return fechaLectura;
	}

	public void setFechaLectura(LocalDate fechaLectura) {
		this.fechaLectura = fechaLectura;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
}
