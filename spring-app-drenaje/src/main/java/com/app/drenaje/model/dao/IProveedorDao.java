package com.app.drenaje.model.dao;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.app.drenaje.model.entity.Proveedor;

public interface IProveedorDao extends JpaRepository<Proveedor, Long> {
	
	@Query("select p from Proveedor p where p.nombreCompleto like %?1%")
	public List<Proveedor> findByNombreCompleto(String term);

}
