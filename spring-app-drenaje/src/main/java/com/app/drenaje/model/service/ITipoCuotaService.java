package com.app.drenaje.model.service;

import java.util.List;
import com.app.drenaje.model.entity.TipoCuota;

public interface ITipoCuotaService {
	
	public TipoCuota store(TipoCuota tipoCuota);
	public List<TipoCuota> getAll();

}
