package com.app.drenaje.model.service;

import java.util.List;

import com.app.drenaje.model.entity.Cuota;

public interface ICuotaService {

	 public List<Cuota>  store(List<Cuota> cuota);
	 public List<Cuota> findByStatusEstadoCuota();
	 public List<Cuota> findCuotaFilterBySocioNombre(String term);
	 public Cuota fidById(Long id);
	 public List<Cuota> findAllByTipoCuota(String tipoCuota);
	 public List<Cuota> findAllCuotaByTipoCuota(String tipoCuota);
	 public List<Cuota> findByEstadoCuotaPendiente(String tipoCuota);
}
