package com.app.drenaje.model.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "gastos")
public class Gasto implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Size(max = 15)
	@Pattern(regexp = "[0-9a-zA-z ]*")
	@Column(name = "no_factura", length = 15)
	private String noFactura;
	private LocalDate fecha;
	private LocalTime hora;
	@ManyToOne(fetch = FetchType.LAZY)
	private Proveedor proveedor;
	@ManyToOne(fetch = FetchType.LAZY)
	private Usuario usuario;
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler", "items" })
	@JoinColumn(name = "gasto_id")
	private List<ItemGasto> items;

	@PrePersist
	private void prePersist() {
		// fecha = LocalDate.now(ZoneId.of("America/Guatemala"));
		// hora = LocalTime.now(ZoneId.of("America/Guatemala"));

		// *************Se calcula fecha y hora de las siguiente manera por diferencia de zona horaria en docker*********************************
		LocalDateTime fh = LocalDateTime.now().minusHours(6);
		fecha = LocalDate.of(fh.getYear(), fh.getMonthValue(), fh.getDayOfMonth());
		hora = LocalTime.of(fh.getHour(), fh.getMinute(), fh.getSecond(), fh.getNano());
		// **************************************************************************************************************************************
	}

	public Long getI() {
		return id;
	}

	public void setI(Long id) {
		this.id = id;
	}

	public String getNoFactura() {
		return noFactura;
	}

	public void setNoFactura(String noFactura) {
		this.noFactura = noFactura;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public LocalTime getHora() {
		return hora;
	}

	public void setHora(LocalTime hora) {
		this.hora = hora;
	}

	public Proveedor getProveedor() {
		return proveedor;
	}

	public void setProveedor(Proveedor proveedor) {
		this.proveedor = proveedor;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<ItemGasto> getItems() {
		return items;
	}

	public void setItems(List<ItemGasto> items) {
		this.items = items;
	}

	public Double getTotal() {
		Double total = 0.00;
		for (ItemGasto item : items) {
			total += item.getSubTotalCompra();
		}

		return total;
	}

}
