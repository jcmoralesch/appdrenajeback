package com.app.drenaje.model.service;

import java.util.List;

import com.app.drenaje.model.entity.InformeSaldo;

public interface IInformeSaldoService {
	
	public InformeSaldo store(InformeSaldo informeSaldo);
	public Integer countInformeSaldo();
	public Long getByMaxId();
	public InformeSaldo findById(Long id);
	public Double sumTotalGasto();
	public Double sumTotalRecaudo();
	public List<InformeSaldo> getAllOrderByDesc();

}
