package com.app.drenaje.model.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.drenaje.model.dao.ITipoCuotaDao;
import com.app.drenaje.model.entity.TipoCuota;

@Service
public class TipoCuotaServiceImpl implements ITipoCuotaService{
	
	@Autowired
	private ITipoCuotaDao tipoCuotaDao;

	@Override
	@Transactional
	public TipoCuota store(TipoCuota tipoCuota) {	
		return tipoCuotaDao.save(tipoCuota);
	}

	@Override
	@Transactional(readOnly = true)
	public List<TipoCuota> getAll() {
		return tipoCuotaDao.findAllOrderByIdDesc();
	}

}
