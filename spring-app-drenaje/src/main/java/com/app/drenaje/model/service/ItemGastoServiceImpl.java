package com.app.drenaje.model.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.app.drenaje.model.dao.IItemGastoDao;

@Service
public class ItemGastoServiceImpl implements IItemGastoService{
	
	@Autowired
	private IItemGastoDao itemGastoDao;

	@Override
	public Double sumTotal() {
		
		return itemGastoDao.sumTotal();
	}

	

}
