package com.app.drenaje.model.dao;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.app.drenaje.model.entity.Gasto;

public interface IGastoDao extends JpaRepository<Gasto, Long> {
	
	@Query("select g from Gasto g where fecha=?1 order by g.id desc")
	public List<Gasto> findByFechaCurrentDate(LocalDate fecha1);
	public List<Gasto> findAllByFechaBetween(LocalDate fecha1, LocalDate fecha2);
    
}
