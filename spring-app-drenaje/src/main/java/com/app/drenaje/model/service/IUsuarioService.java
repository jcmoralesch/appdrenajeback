package com.app.drenaje.model.service;

import com.app.drenaje.model.entity.Usuario;

public interface IUsuarioService {
	public Usuario findByUsername(String username);

}
