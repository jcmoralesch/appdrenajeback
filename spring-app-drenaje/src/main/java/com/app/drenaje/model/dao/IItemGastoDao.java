package com.app.drenaje.model.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import com.app.drenaje.model.entity.ItemGasto;

public interface IItemGastoDao extends JpaRepository<ItemGasto, Long>{
	
	@Query("select sum(i.subTotalCompra) from ItemGasto i")
	public Double sumTotal();

}
