package com.app.drenaje.model.service;

import java.time.LocalDate;
import java.util.List;
import com.app.drenaje.model.entity.Cuota;
import com.app.drenaje.model.entity.PagoCuota;

public interface IPagoCuotaService {
	public List<PagoCuota> findByFechaPago(LocalDate fecha1);
	public List<PagoCuota> storePagoUpdateCuota(List<PagoCuota> pagoCuota,List<Cuota> cuota);
	public List<PagoCuota> finByFechaBetween(LocalDate fecha1,LocalDate fecha2);
	public List<PagoCuota> findPagoCuotaByDateAndTipoCuota(LocalDate fecha1,LocalDate fecha2,String tipoCuota);
	public Double sumCantidad();
	public List<PagoCuota> findAll();
	
	public List<PagoCuota> findAllByCuotaSocioId(Long id);

}
