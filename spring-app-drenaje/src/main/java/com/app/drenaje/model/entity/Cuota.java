package com.app.drenaje.model.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;

@Entity
@Table(name = "cuotas")
public class Cuota implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@DecimalMin(value = "0.00")
	@Column(nullable = false)
	private Double cantidad;
	@Min(value = 1)
	@Column(name = "no_pagos", nullable = false)
	private Integer noPagos;
	@Column(name = "total_pagado", nullable = false)
	private Double totalPagado = 0.00;
	@Column(nullable = false)
	private Double restante;
	@ManyToOne
	private TipoCuota tipoCuota;
	@ManyToOne
	private Socio socio;
	private LocalDate fechaAcordada;
	@ManyToOne
	private Usuario usuario;
	@Column(name = "estado_cuota", length = 12)
	private String estadoCuota = "Pendiente";
	@Column(name = "estado_pago", length = 2)
	private String estadoPago = "P";

	@PrePersist
	private void prePersist() {
		// fechaAcordada = LocalDate.now(ZoneId.of("America/Guatemala"));

		// *************Se calcula fecha y hora de las siguiente manera por diferencia de zona horaria en docker*********************************
		LocalDateTime fh = LocalDateTime.now().minusHours(6);
		fechaAcordada = LocalDate.of(fh.getYear(), fh.getMonthValue(), fh.getDayOfMonth());
		// **************************************************************************************************************************************
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getCantidad() {
		return cantidad;
	}

	public void setCantidad(Double cantidad) {
		this.cantidad = cantidad;
	}

	public Integer getNoPagos() {
		return noPagos;
	}

	public void setNoPagos(Integer noPagos) {
		this.noPagos = noPagos;
	}

	public Double getTotalPagado() {
		return totalPagado;
	}

	public void setTotalPagado(Double totalPagado) {
		this.totalPagado = totalPagado;
	}

	public Double getRestante() {
		return restante;
	}

	public void setRestante(Double restante) {
		this.restante = restante;
	}

	public TipoCuota getTipoCuota() {
		return tipoCuota;
	}

	public void setTipoCuota(TipoCuota tipoCuota) {
		this.tipoCuota = tipoCuota;
	}

	public Socio getSocio() {
		return socio;
	}

	public void setSocio(Socio socio) {
		this.socio = socio;
	}

	public LocalDate getFechaAcordada() {
		return fechaAcordada;
	}

	public void setFechaAcordada(LocalDate fechaAcordada) {
		this.fechaAcordada = fechaAcordada;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public String getEstadoCuota() {
		return estadoCuota;
	}

	public void setEstadoCuota(String estadoCuota) {
		this.estadoCuota = estadoCuota;
	}

	public String getEstadoPago() {
		return estadoPago;
	}

	public void setEstadoPago(String estadoPago) {
		this.estadoPago = estadoPago;
	}

}
