package com.app.drenaje.model.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.app.drenaje.model.dao.IInformeSaldoDao;
import com.app.drenaje.model.entity.InformeSaldo;

@Service
public class InformeSaldoServiceImpl implements IInformeSaldoService{
	
	@Autowired
	private IInformeSaldoDao informeSaldoDao;

	@Override
	@Transactional
	public InformeSaldo store(InformeSaldo informeSaldo) {
		
		return informeSaldoDao.save(informeSaldo);
	}

	@Override
	@Transactional( readOnly = true)
	public Integer countInformeSaldo() {
		
		return informeSaldoDao.countInformeSaldo();
	}

	@Override
	@Transactional(readOnly = true)
	public Long getByMaxId() {
		
		return informeSaldoDao.getByMaxId();
	}

	@Override
	public InformeSaldo findById(Long id) {
		
		return informeSaldoDao.findById(id).orElse(null);
	}

	@Override
	@Transactional(readOnly = true)
	public Double sumTotalGasto() {
		
		return informeSaldoDao.sumTotalGasto();
	}

	@Override
	@Transactional(readOnly = true)
	public Double sumTotalRecaudo() {
		
		return informeSaldoDao.sumTotalRecaudo();
	}

	@Override
	@Transactional(readOnly = true)
	public List<InformeSaldo> getAllOrderByDesc() {
		
		return informeSaldoDao.getAllOrderByDesc();
	}

}
