package com.app.drenaje.model.dao;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import com.app.drenaje.model.entity.Cuota;

public interface ICuotaDao extends JpaRepository<Cuota, Long> {
	
	@Query("select c from Cuota c where c.estadoCuota='Pendiente'")
	public List<Cuota> findByStatusEstadoCuota();
	
	@Query("select c from Cuota c where (c.socio.nombre like %?1% or c.socio.apellido like %?1%) and c.estadoPago='P' and estadoCuota='Pendiente'")
	public List<Cuota> findCuotaFilterBySocioNombre(String term);
	
	@Query("select c from Cuota c where c.tipoCuota.tipoCuota=?1 and c.estadoCuota='Pendiente'")
	public List<Cuota> findAllByTipoCuota(String tipoCuota);
	
	@Query("select c from Cuota c where c.tipoCuota.tipoCuota=?1")
	public List<Cuota> findAllCuotaByTipoCuota(String tipoCuota);
	
	@Query("select c from Cuota c where c.tipoCuota.tipoCuota=?1 and c.estadoPago='P' and estadoCuota='Pendiente'")
	public List<Cuota> findByEstadoCuotaPendiente(String tipoCuota);

}
