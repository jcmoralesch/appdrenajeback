package com.app.drenaje.model.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.app.drenaje.model.dao.ISectorDao;
import com.app.drenaje.model.entity.Sector;

@Service
public class SectorServiceImpl implements ISectorService {
	
	@Autowired
	private ISectorDao sectorDao;

	@Override
	@Transactional
	public Sector store(Sector sector) {
		
		return sectorDao.save(sector);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Sector> getAll() {
		
		return sectorDao.findAll();
	}

}
