package com.app.drenaje.model.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.drenaje.model.dao.ICuotaDao;
import com.app.drenaje.model.entity.Cuota;

@Service
public class CuotaServiceImpl implements ICuotaService{
	
	@Autowired
	private ICuotaDao cuotaDao;

	@Override
	@Transactional
	public List<Cuota> store(List<Cuota> cuota) {
		
		return cuotaDao.saveAll(cuota);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Cuota> findByStatusEstadoCuota() {
		
		return cuotaDao.findByStatusEstadoCuota();
	}

	@Override
	@Transactional(readOnly = true)
	public List<Cuota> findCuotaFilterBySocioNombre(String term) {
		
		return cuotaDao.findCuotaFilterBySocioNombre(term);
	}

	@Override
	@Transactional(readOnly = true)
	public Cuota fidById(Long id) {
		
		return cuotaDao.findById(id).orElse(null);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Cuota> findAllByTipoCuota(String tipoCuota) {
		
		return cuotaDao.findAllByTipoCuota(tipoCuota);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Cuota> findAllCuotaByTipoCuota(String tipoCuota) {
		
		return cuotaDao.findAllCuotaByTipoCuota(tipoCuota);
	}

	@Override
	@Transactional( readOnly = true)
	public List<Cuota> findByEstadoCuotaPendiente(String tipoCuota) {
		
		return cuotaDao.findByEstadoCuotaPendiente(tipoCuota);
	}

}
