package com.app.drenaje.model.service;

import java.time.LocalDate;
import java.util.List;
import com.app.drenaje.model.entity.Gasto;

public interface IGastoService {
	
	public Gasto store(Gasto gasto);
	public List<Gasto> findByFechaCurrentDate(LocalDate fecha1);
	public List<Gasto> findAllByFechaBetween(LocalDate fecha1, LocalDate fecha2);

}
