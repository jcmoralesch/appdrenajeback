package com.app.drenaje.model.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.app.drenaje.model.entity.TipoCuota;

public interface ITipoCuotaDao extends JpaRepository<TipoCuota, Long> {
	
	@Query("select t from TipoCuota t order by t.id desc")
	public List<TipoCuota> findAllOrderByIdDesc();

}
