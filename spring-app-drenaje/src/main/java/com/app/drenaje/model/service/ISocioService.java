package com.app.drenaje.model.service;

import java.util.List;
import com.app.drenaje.model.entity.Socio;

public interface ISocioService {
	
	public Socio store(Socio socio);
	public List<Socio> findAllByStatus(String status);
	public List<Socio> findAllBySector(Integer sector);
	public List<Socio> findAllByGenero(String genero);
	public List<Socio> findAllBySectorAndSexo(String genero,Integer sector);
	public Socio findById(Long id);

}
