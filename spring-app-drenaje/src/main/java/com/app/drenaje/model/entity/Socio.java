package com.app.drenaje.model.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Entity
@Table(name = "socios")
public class Socio implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@NotEmpty
	@Size(max = 20)
	@Column(length = 20, unique = true)
	@Pattern(regexp = "[0-9 ]*")
	private String codigo;
	@NotEmpty
	@Size(max = 40)
	@Column(length = 40)
	private String nombre;
	@NotEmpty
	@Size(max = 40)
	@Column(length = 40)
	private String apellido;
	@Size(max = 10)
	@Column(length = 10)
	private String telefono = "0000 0000";
	@Size(max = 8)
	@Column(length = 8)
	private String sexo;
	private Integer edad;
	private LocalDate fecha;
	@Column(length = 5)
	private String status = "A";
	@ManyToOne
	private Sector sector;

	@PrePersist
	private void prepersist() {
		// fecha = LocalDate.now(ZoneId.of("America/Guatemala"));
		// *************Se calcula fecha y hora de las siguiente manera por diferencia de zona horaria en docker************
		LocalDateTime fh = LocalDateTime.now().minusHours(6);
		fecha = LocalDate.of(fh.getYear(), fh.getMonthValue(), fh.getDayOfMonth());
		// **************************************************************************************************************************************
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public Integer getEdad() {
		return edad;
	}

	public void setEdad(Integer edad) {
		this.edad = edad;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Sector getSector() {
		return sector;
	}

	public void setSector(Sector sector) {
		this.sector = sector;
	}

}
